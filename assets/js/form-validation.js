// Form validation - register form
toastr.options = {"positionClass": "toast-top-full-width"};
$('.form-register').validate({
  messages: {
    inf_field_FirstName: "Please enter your first name",
    password: {
      required: "Please provide a password",
      minlength: "Your password must be at least 5 characters long"
    },
    confirmPassword: {
      required: "Please provide a password",
      minlength: "Your password must be at least 5 characters long",
      equalTo: "Please enter the same password as above"
    },
    inf_field_LastName: "Please enter your last name",
    inf_field_Email: "Please enter a valid email address",
    inf_field_Phone1: "Please enter your phone number",
    inf_field_Company: "Please enter your company name"
  },      
  submitHandler: function(form) {
    var $this = $(form);
    $.ajax({
      url: $this.attr('action'),
      type: 'POST',
      data: $this.serialize(),
    })
    .done(function(msg) {
      if( msg == 'ok' ) {
        toastr.success('Thank you for signing up. Our manager will contact you soon!');
        $this[0].reset();
      } else {
        toastr.error('An error occured. Please try again later.');
      }
    })
    .fail(function() {
      // toastr.error('An error occured. Please try again later.');
      toastr.success('Thank you for signing up. Our manager will contact you soon!');
      $this[0].reset();
      location.replace("http://witnation.com"); //REDIRECT HERE TO WHATEVER IS NEEDED
    });
  }      
});
